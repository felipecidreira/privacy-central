# Dogma Data Privacy

## Central da Privacidade
A Central da Privacidade oferece acesso, por parte dos titulares de dados, às principais funcionalidades da Dogma:

1. Gestão de Cookies
2. Gestão de Consentimentos
3. Pedidos (acesso, remoção e retificação de dados)

### Inserção no Site
É fácil inserir a Central da Privacidade no seu site. Idealmente, a Central da Privacidade deve estar presente em todas as páginas.

### Customização
É possível customizar a Central da Privacidade para as suas necessidades.  
As principais formas de customização são:

1. Temas
2. Estilo
3. Logo

### API Javascript
É possível interagir com a Central da Privacidade via javascript em qualquer página que tenha o código da Central da Privacidade instalado.

Todas as páginas com o código da Central da Privacidade exportam a API Javascript da dogma através do objeto 'dogma' no escopo global da janela.  

```javascript
window.dogma
```

As principais funcionalidades disponíveis são as seguintes:

1. Cookies
2. Consentimentos
3. Pedidos
4. Banner

#### Cookies
```javascript
const cookies = window.dogma.cookies()
```

O objeto _cookies_ tem os seguintes métodos:  

2. _list()_ para listar os grupos e cookies existentes
1. _accept()_ para aceitar ou recusar um cookie e/ou grupo de cookies
3. _onChange()_ chamada de _callback_ quando um cookie e/ou grupo é aceito ou recusado

##### Listando os Cookies existentes
Para listar os cookies cadastrados na plataforma use o método _list()_ do objeto cookies. 

```javascript 
await window.dogma.cookies().list().then(r => r.json())
```

A chamada retorna a lista de grupos e cookies em uma _Promise_. Exemplo:
```javascript
[
    {
        "id": 1,
        "name": "Nome do Grupo",
        "description": "Descrição do Grupo",
        "required": true, // indica se todos os cookies deste grupo são obrigatórios
        "preferences":[{   
            "id"        :1,
            "accepted"  :false,
            "name"      :"nome_do_cookie",
            "provider"  :"dominio.do.cookie.com.br",
            "kind"      :"http",
            "duration"  :"unlimited",
            "purpose"   :"descrição do propósito"
        }]
    }
]
```

#####  Aceitando ou recusando Grupos de Cookies ou Cookies individualmente
Para aceitar ou recusar listar os cookies cadastrados na plataforma use o método accept(accepted, group, cookie)_ do objeto cookies. 

Parâmetros:  

 *  _accepted (**boolean**): indica se o grupo/cookie foi aceito ou recusado
 *  _group_  (**number**): o id do grupo
 *  _cookie_ (**number**): o id do cookie. Este parâmetro é opcional. Caso seja omitido o valor de _accepted_ será aplicado à todos os cookies do grupo

Exemplos:  

Para aceitar todos os cookies do grupo 1
```javascript 
await window.dogma.cookies().accept(true, 1)
```

Para recusar apenas o cookie 99 do grupo 42
```javascript 
await window.dogma.cookies().accept(false, 42, 99)
```

**IMPORTANTE**
```  
    O aceite ou recusa de um de um grupo e/ou cookie  não implica na remoção e futura não utilização 
    do cookie por parte do site, uma vez que alguns cookies não podem ser removidos via javascript.

    Leia a seção "Recebendo Notificações de aceite ou recusa de Grupos e Cookies" para mais informações.
```

##### Recebendo Notificações de aceite ou recusa de Grupos e Cookies
registre uma função de _callback_  no método _onChange(callback)_ para ser notificado quando um titular de dados aceita ou recusa a utilização de um grupo e/ou cookie.
```javascript 

funtion myHandler(evt) {
    console.log(`Evento: ${evt.event}`)
    console.log(`Sucesso: ${evt.success}`)
    console.log(`Resposta: ${evt.response}`)
    console.log(`Dados: ${evt.data}`)
    console.log(`Erro: ${evt.error}`)
}

window.dogma.cookies().onChange(myHandler)
```



#### Consentimentos
```javascript
const consent = window.dogma.consent()
```

O objeto _consent_ tem os seguintes métodos:  

1. _accept()_ 
2. _list()_
3. _onChange()_

#### Pedidos
```javascript
const inquiries = window.dogma.inquiries()
```

O objeto _inquiries_ tem os seguintes métodos:  

1. _list()_


#### Banner
```javascript
const banner = window.dogma.banner()
```

O objeto _banner_ tem os seguintes métodos:  

1. _isClosed()_
2. _open()_
3. _close()_


